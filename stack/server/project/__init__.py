import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # MULTI POSTGRES
    # db_uri_db1 = os.environ.get('SQLALCHEMY_DATABASE_URI_DB1')
    # db_uri_db2 = os.environ.get('SQLALCHEMY_DATABASE_URI_DB2')
    # db_uri_db3 = os.environ.get('SQLALCHEMY_DATABASE_URI_DB3')

    # app.config['SQLALCHEMY_DATABASE_URI_DB1'] = db_uri_db1
    # app.config['SQLALCHEMY_DATABASE_URI_DB2'] = db_uri_db2
    # app.config['SQLALCHEMY_DATABASE_URI_DB3'] = db_uri_db3

    # ONE POSTGRES
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')

    # instantiate the extensions
    db = SQLAlchemy()
    migrate = Migrate()

    # enable CORS
    CORS(app)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)
    migrate.init_app(app, db)

    # register blueprints
    from project.api.books import books_blueprint
    app.register_blueprint(books_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
