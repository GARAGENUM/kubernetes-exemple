# Kubernetes cluster avec Vagrant

![vagrant](https://www.vectorlogo.zone/logos/vagrantup/vagrantup-official.svg)


## Pré requis

- Virtualbox
- [Kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)
- [Vagrant](https://developer.hashicorp.com/vagrant/tutorials/getting-started?product_intent=vagrant)

### Install Vagrant

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update -y && sudo apt install vagrant -y
```

### config Virtualbox

```bash
sudo mkdir -p /etc/vbox/
echo "* 0.0.0.0/0 ::/0" | sudo tee -a /etc/vbox/networks.conf
```

### Créer cluster :hammer:

```bash
vagrant up
```

> :coffee: Laisser le temps à Vagrant de construire les machines :watch: 5 - 10 min approximativement

### Kube config

```bash
cp configs/config ~/.kube/
```

> On récupère la config du master pour intéragir directement avec la commande kubectl

### Déployer app :rocket:

```bash
kubectl apply -f kubernetes/devops.yml
```

- commandes utiles:
```bash
kubectl get nodes -o wide
kubectl get svc
kubectl get all
```

### Load balancer

- Installation de metallb:
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
```

> Cette config convient pour créer des services de load-balancer pour les cluster bare metal (non cloud)

doc: https://metallb.universe.tf/installation/

- Appliquer les configs pour metallb:
```bash
kubectl apply -f kubernetes/ipaddresspool.yml
kubectl apply -f kubernetes/l2advertisement.yml
```

> Définit les adresses IP utilisées par les services exposant les applicatons

- Modifier son fichier /etc/hosts:
```bash
echo '10.0.0.50 k8s.exemple' | sudo tee -a /etc/hosts
```

>> http://k8s-exemple

### Dashboard

- Obtenir un jeton pour s'authentifier à la dashboard:
```bash
kubectl -n kubernetes-dashboard get secret/admin-user -o go-template="{{.data.token | base64decode}}"
kubectl proxy
```

Adresse de la dashboard: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=kubernetes-dashboard

>> copier le jeton pour accéder à la dashboard
